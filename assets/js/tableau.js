(async function($) {
  const charts = document.querySelectorAll('tableau-viz');

  if (charts.length === 0) {
    console.warn('No charts embedded on page, skipping...');
    return;
  }

  const embedSdk = document.createElement('script');
  embedSdk.type = 'module';
  embedSdk.src = 'https://10az.online.tableau.com/javascripts/api/tableau.embedding.3.latest.min.js';

  document.body.appendChild(embedSdk);

  const res = await fetch('https://us-central1-mcottrell-8f2b9454.cloudfunctions.net/tableau-connected-app');
  const data = await res.json();

  charts.forEach((el) => {
    if (el.getAttribute('src').startsWith('https://us-west-2b.online.tableau.com/t/gitlabpublic/')) {
      el.setAttribute('token', data.token);
    }
  });
})(jQuery);
