module gitlab.com/gitlab-com/content-sites/docsy-gitlab

go 1.19

require (
	github.com/google/docsy v0.8.0 // indirect
	github.com/google/docsy/dependencies v0.7.2 // indirect
)
